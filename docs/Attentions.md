## 一. 注意事项

<h3>
   
   * 参考[任务配置属性](Web-Console-Config.md)对新建的任务进行参数配置，运行模式选中 "BEAN模式"，JobWorker属性填写任务注解“@JobWorker”中定义的值。如下图：
 <img src="images/Attention-jobworker.png"/>
   
   <br/>
   * 新建执行器UI界面中的AppName的值应该和phoenix.job.executor.appname属性配置的值一致。如下图：
<img src="images/Attention-executor.png"/>
<br/>
<b>建议【注册方式】使用手动模式。</b>

</h3>

## 二. 出现jetty版本冲突解决方案：统一版本9.2.24.v20180105

		<properties>
			<job.version>1.0.0-SNAPSHOT</job.version>
			<jetty-server.version>9.2.24.v20180105</jetty-server.version>
		</properties>
		
		<dependencies>
		
		//省略其他jar依赖
		
			<!-- jetty for test -->
			<dependency>
				<groupId>org.eclipse.jetty</groupId>
				<artifactId>jetty-server</artifactId>
				<scope>test</scope>
			</dependency>
			<dependency>
				<groupId>org.eclipse.jetty</groupId>
				<artifactId>jetty-webapp</artifactId>
				<version>${jetty-server.version}</version>
				<scope>test</scope>
				<exclusions>
					<exclusion>
						<artifactId>jetty-server</artifactId>
						<groupId>org.eclipse.jetty</groupId>
					</exclusion>
				</exclusions>
			</dependency>
			<dependency>
				<groupId>org.eclipse.jetty</groupId>
				<artifactId>jetty-jsp</artifactId>
				<version>${jetty-server.version}</version>
				<scope>test</scope>
			</dependency>
			
			<!-- 定时任务管理平台依赖jar -->
			<dependency>
				<groupId>org.eclipse.jetty</groupId>
				<artifactId>jetty-server</artifactId>
				<version>${jetty-server.version}</version>
			</dependency>
			<dependency>
				<groupId>com.bestone.job</groupId>
				<artifactId>phoenix-job-executor-client</artifactId>
				<version>${job.version}</version>
				<exclusions>
					<exclusion>
						<artifactId>jetty-server</artifactId>
						<groupId>org.eclipse.jetty</groupId>
					</exclusion>
				</exclusions>
			</dependency>
		</dependencies>