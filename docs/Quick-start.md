# 部署步骤

  * 一. 下载并部署[web console项目](wars/phoenix-job-admin.war).(可省略)
    * 修改属性配置文件：部署之前需要修改war中phoenix-job-admin.properties中的参数值
  	  * 数据库配置 <br/>
	phoenix.job.db.driverClass=com.mysql.jdbc.Driver <br/>
	phoenix.job.db.url=jdbc:mysql://ip:port/phoenix-job?useUnicode=true&characterEncoding=UTF-8 <br/>
	phoenix.job.db.user=root <br/>
	phoenix.job.db.password=root <br/>

  	  * 异常报警邮箱配置 <br/>
    phoenix.job.mail.host=smtp.163.com <br/>
	phoenix.job.mail.port=25 <br/>
	phoenix.job.mail.username=bestone_job@163.com <br/>
	phoenix.job.mail.password=123456 <br/>
	phoenix.job.mail.sendNick=百事通任务管理平台 <br/>

  	  * 控制台登录用户名和密码配置 <br/>
    phoenix.job.login.username=admin <br/>
	phoenix.job.login.password=admin123 <br/>


  * 二. [初始化数据脚步phoenix-job.sql](sql/phoenix-job.sql).(可省略)

<b>`使用开发环境，以上步骤可省略。`</b> <br/>
<b>`开发环境地址：http://47.97.22.177:7777	           用户名：admin  密码：admin123`<b> <br/>
 
  * 三. [执行器开发指南(使用案例)](Executor-Development-Guide.md)  
   1. [内嵌模式](/phoenix-job-executor-embeddable/README.md)
   1. [独立模式](/phoenix-job-executor-launcher/README.md)
   1. [支持dubbo的独立模式](/phoenix-job-executor-launcher-dubbo/README.md)
   1. [集群模式](Deploy-cluster-mode.md)
   
   <b>`获取项目源码：git clone http://git.haolawyers.cn/platformgroup/phonenix-job-executor.git`</b>

  * 四. 访问web console系统，查看UI，恢复任务执行，在调度日志模块中查看任务执行结果。