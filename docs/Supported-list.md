## 环境准备
  * 使用 maven依赖相应的工具包

1. 使用提供的[setting.xml](dependency/setting.xml)
	
2. POM文件中添加如下配置(使用指定setting.xml，该配置可省略)：
	
		
		<repositories>
			<repository>
				<id>public</id>
				<name>Public Repositories</name>
				<url>http://47.97.22.177:8081/repository/maven-public/</url>
			</repository>
		</repositories>  


## 技术规范
  * JDK8+
  * Tomcat 8+
  * Dubbo 2.6.0
  * Springboot 1.5.10.RELEASE
  * Spring 4.3.14.RELEASE
  * Quartz 2.3.0
  * 日志使用log4j2
  * Jetty client/server 9.2.24.v20180105
  * Mysql Driver 5.x+