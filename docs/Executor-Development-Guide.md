## 执行器
   1. [内嵌模式](phoenix-job-executor-embeddable/README.md)
   1. [单机模式](phoenix-job-executor-launcher/README.md)
   1. [单机dubbo模式](phoenix-job-executor-launcher-dubbo/README.md)
   1. [集群模式](docs/Deploy-cluster-mode.md)