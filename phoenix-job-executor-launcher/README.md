#### 简介
独立部署模式（JOB执行器和业务代码部署在不同JVM(不同机器)上的模式）的示例项目。
<b>`该模式适合执行器和定时任务服务分布式架构的项目。`</b>

#### 集成步骤

 * 1.继承"AbstractJobWorker"：“com.bestone.job.executor.service.api.AbstractJobWorker”，并实现任务执行方法doExecute；<br/>
 
 
 * 2.注册到Spring容器：添加“@Component”注解，被Spring容器扫描为Bean实例；<br/>
 
 
 * 3.注册到执行器工厂：添加“@JobWorker(value="自定义jobWorker名称")”注解，注解value值对应的是调度中心新建任务的JobWorker属性的值。<br/>
 
 
 * 4.执行日志：需要通过 "JobLogger.log" 打印执行日志，这里可以输入业务相关的一些参数，方便任务异常的时候排查问题。<br/>


	例如：
	import java.util.concurrent.TimeUnit;
	import org.springframework.stereotype.Service;
	import com.bestone.job.executor.service.api.AbstractJobWorker;
	import com.bestone.job.executor.service.api.JobWorker;
	import com.bestone.job.executor.service.api.Result;
	import com.bestone.job.executor.service.log.JobLogger;
	
	@JobWorker(value = "demoJobWorker2")
	@Service
	public class DemoJobWorker extends AbstractJobWorker {
	
		@Override
		public Result<String> doExecute(String param) throws Exception {
	
			// 模拟业务操作
			System.out.println("phoenix-job-executor-launcher-->>>>job task handled business operation...");
			TimeUnit.SECONDS.sleep(2);
	
			JobLogger.log("PHOENIX-JOB execute successfully.");
			return SUCCESS;
		}
	}


 * 5.配置类，继承com.bestone.job.executor.servce.config.JobConfig。如下：
 
	import org.springframework.context.annotation.Configuration;
	import com.bestone.job.executor.servce.config.JobConfig;
	
	@Configuration
	public class DefaultJobConfig extends JobConfig {
	}


 * 6.配置属性文件application.properties
	
	# web port
	server.port=8082
	
	### phoenix-job admin address list, such as "http://address" or "http://address01,http://address02"
	phoenix.job.admin.addresses=http://127.0.0.1:7777
	
	### phoenix-job executor address
	phoenix.job.executor.appname=fashang-standalone-job-executor-1
	phoenix.job.executor.ip=
	phoenix.job.executor.port=9988
	
	### phoenix-job, access token
	phoenix.job.accessToken=
	
	### phoenix-job log path
	phoenix.job.executor.logpath=/data/applogs/phoenix-job/jobhandler
	### phoenix-job log retention days
	phoenix.job.executor.logretentiondays=-1
	


 * 7.编写启动类，如下：
 
	import org.springframework.boot.SpringApplication;
	import org.springframework.boot.autoconfigure.SpringBootApplication;
	import org.springframework.context.annotation.ComponentScan;
	
	@SpringBootApplication
	@ComponentScan(basePackages = "com.bestone.job.executor")
	public class Application {
	
		public static void main(String[] args) {
	        SpringApplication.run(Application.class, args);
		}
	}
	
#### 部署
直接运行com.bestone.job.executor.Application即可。

<b>`线上打包成可执行jar部署`</b>