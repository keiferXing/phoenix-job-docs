#### 简介
定时任务管理平台客户端项目和集成实例项目。支持：embedded模式，standalone模式(duboo/springboot)。


- [客户端项目 phoenix-job-executor-client](http://git.haolawyers.cn/platformgroup/phonenix-job-executor/tree/release/phoenix-job-executor-client)


- [内嵌模式项目集成示例 phoenix-job-executor-embeddable](http://git.haolawyers.cn/platformgroup/phonenix-job-executor/tree/release/phoenix-job-executor-embeddable)

- [standalone模式项目集成示例 phoenix-job-executor-launcher](http://git.haolawyers.cn/platformgroup/phonenix-job-executor/tree/release/phoenix-job-executor-launcher)

- [standalone模式支持dubbo项目集成示例  phoenix-job-executor-launcher-dubbo](http://git.haolawyers.cn/platformgroup/phonenix-job-executor/tree/release/phoenix-job-executor-launcher-dubbo)

## [使用文档](docs/README.md)	

## Standalone模式支持dubbo项目集成示例 

- [接口项目 phoenix-job-dubbo-demo-api](http://git.haolawyers.cn/platformgroup/phonenix-job-executor/tree/release/phoenix-job-executor-launcher-dubbo/phoenix-job-dubbo-demo-api)
- [dubbo服务提供者项目 phoenix-job-dubbo-demo-provider](http://git.haolawyers.cn/platformgroup/phonenix-job-executor/tree/release/phoenix-job-executor-launcher-dubbo/phoenix-job-dubbo-demo-provider)
- [业务执行器项目 phoenix-job-dubbo-demo-consumer](http://git.haolawyers.cn/platformgroup/phonenix-job-executor/tree/release/phoenix-job-executor-launcher-dubbo/phoenix-job-dubbo-demo-consumer)
