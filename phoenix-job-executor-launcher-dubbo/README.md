#### 简介
独立部署模式集成dubbo的示例项目。
<b>`该模式适合执行器和定时任务提供dubbo服务的分布式架构项目。`</b>

#### 项目简介
接口项目：phoenix-job-dubbo-demo-api，定义接口申明；<br/>
接口业务实现项目：phoenix-job-dubbo-demo-provider，提供dubbo提供服务；<br/>
执行器实现项目：phoenix-job-dubbo-demo-provider，定时任务中消费dubbo服务。<br/>

#### POM依赖
	<properties>
		<dubbo.springboot.starter>1.0.2</dubbo.springboot.starter>
		<dubbo.version>2.5.8</dubbo.version>
		<job.executor.client.version>1.0.0-SNAPSHOT</job.executor.client.version>
	</properties>
	
	<dependencies>
		
		<!-- spring-boot-starter-web (spring-webmvc + tomcat) -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<version>${spring-boot.version}</version>
			<exclusions>
				<exclusion>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-starter-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		
		<!-- log4j2 dependency -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-log4j2</artifactId>
			<version>${spring-boot.version}</version>
		</dependency>

		<!-- dubbo dependency -->
		<dependency>
			<groupId>com.alibaba.spring.boot</groupId>
			<artifactId>dubbo-spring-boot-starter</artifactId>
			<version>${dubbo.springboot.starter}</version>
		</dependency>
		<dependency>
			<groupId>com.alibaba</groupId>
			<artifactId>dubbo</artifactId>
			<version>${dubbo.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.zookeeper</groupId>
			<artifactId>zookeeper</artifactId>
			<version>3.4.8</version>
		</dependency>
		<dependency>
			<groupId>com.101tec</groupId>
			<artifactId>zkclient</artifactId>
			<version>0.1.0</version>
		</dependency>

		<!-- job dependency -->
		<dependency>
			<groupId>com.bestone.job</groupId>
			<artifactId>phoenix-job-executor-client</artifactId>
			<version>${job.executor.client.version}</version>
		</dependency>
	</dependencies>


#### dubbo服务项目phoenix-job-dubbo-demo-provider
   1.Springboot配置文件application.properties
	
	#dubbo config
	spring.dubbo.appname=phoenix-job-dubbo-demo-provider
	spring.dubbo.registry=zookeeper://127.0.0.1:2181
	spring.dubbo.protocol=dubbo
	spring.dubbo.port=20880
	
	#tomcat port
	server.port=8011

 2.暴露dubbo服务
	
	import org.springframework.stereotype.Component;
	import com.alibaba.dubbo.config.annotation.Service;
	import com.bestone.job.demo.service.api.MessageService;
	
	@Service(interfaceClass = MessageService.class, version = "1.0.0")
	@Component
	public class MessageServiceImpl implements MessageService {
		@Override
		public String sendMsg(String message) {
			return "job task send message : " + message;
		}
	}
 
 3.启动项目
	
	@SpringBootApplication
	@EnableDubboConfiguration
	public class DubboProviderLauncher {
		public static void main(String[] args) {
			SpringApplication.run(DubboProviderLauncher.class, args);
		}
	}


#### 执行器项目phoenix-job-dubbo-demo-consumer


1.任务工作类com.bestone.job.executor.service.jobworker.DemoJobWorker
 * 1).继承"AbstractJobWorker"：“com.bestone.job.executor.service.api.AbstractJobWorker”，并实现任务执行方法doExecute；
 
 * 2).注册到Spring容器：添加“@Component”注解，被Spring容器扫描为Bean实例；
 
 * 3).注册到执行器工厂：添加“@JobWorker(value="自定义jobWorker名称")”注解，注解value值对应的是调度中心新建任务的JobWorker属性的值。
 
 * 4).执行日志：需要通过 "JobLogger.log" 打印执行日志，这里可以输入业务相关的一些参数，方便任务异常的时候排查问题。

	 例如：
	import java.util.UUID;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Service;
	import com.bestone.job.demo.service.PrintMessageService;
	import com.bestone.job.executor.service.api.AbstractJobWorker;
	import com.bestone.job.executor.service.api.JobWorker;
	import com.bestone.job.executor.service.api.Result;
	import com.bestone.job.executor.service.log.JobLogger;
	
	@JobWorker(value = "demoDubboJobWorker")
	@Service
	public class DemoJobWorker extends AbstractJobWorker {
	
		@Autowired
		PrintMessageService printMessageService;
	
		@Override
		public Result<String> doExecute(String param) throws Exception {
	
			param = "dubboJobWorker" + UUID.randomUUID();
			printMessageService.print(param);
	
			JobLogger.log("PHOENIX-JOB execute successfully with params [ " + param + " ]");
			return SUCCESS;
		}
	}

2.配置类，继承com.bestone.job.executor.servce.config.JobConfig。如下：
 
	import org.springframework.context.annotation.Configuration;
	import com.bestone.job.executor.servce.config.JobConfig;
	
	@Configuration
	public class DefaultJobConfig extends JobConfig {
	}

3.配置属性文件application.properties
	
	### dubbo config
	spring.dubbo.appname=phoenix-job-dubbo-demo-consumer
	spring.dubbo.registry=zookeeper://127.0.0.1:2181
	spring.dubbo.protocol=dubbo
	
	### web port
	server.port=8787
	
	### phoenix-job admin address list, such as "http://address" or "http://address01,http://address02"
	phoenix.job.admin.addresses=http://127.0.0.1:7777
	
	### phoenix-job executor address
	phoenix.job.executor.appname=fashang-dubbo-job-executor-1
	phoenix.job.executor.ip=
	phoenix.job.executor.port=9988
	
	### phoenix-job, access token
	phoenix.job.accessToken=
	
	### phoenix-job log path
	phoenix.job.executor.logpath=/data/applogs/phoenix-job/jobhandler
	### phoenix-job log retention days
	phoenix.job.executor.logretentiondays=-1

	
4.定时任务实现类，如下：
	
	import org.springframework.stereotype.Component;
	import com.alibaba.dubbo.config.annotation.Reference;
	import com.bestone.job.demo.service.api.MessageService;
	@Component
	public class PrintMessageService {
	
		@Reference(interfaceClass = MessageService.class, version = "1.0.0")
		private MessageService messageService;
	
		public void print(String message) {
			messageService.sendMsg("-------job was executed-----,to send message:" + message);
		}
	}


5.启动类，如下：

	import org.springframework.boot.SpringApplication;
	import org.springframework.boot.autoconfigure.SpringBootApplication;
	import org.springframework.context.annotation.ComponentScan;
	
	import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
	
	@SpringBootApplication
	@EnableDubboConfiguration
	@ComponentScan(basePackages = "com.bestone.job")
	public class DubboConsumerLauncher {
		public static void main(String[] args) {
			SpringApplication.run(DubboConsumerLauncher.class, args);
		}
	}
	
#### 部署
	1.先运行zookeeper，默认端口2181
	2.运行phoenix-job-dubbo-demo-provider项目中的com.bestone.job.demo.DubboProviderLauncher
	3.运行phoenix-job-dubbo-demo-consumer项目中的com.bestone.job.demo.DubboConsumerLauncher

<b>`线上打包成可执行jar部署`</b>