#### 简介
JOB执行器客户端，支持2种模式集成：内嵌模式和独立模式。<br/>
所有业务任务必须继承自com.bestone.job.executor.service.api.AbstractJobWorker，实现其中的doExecute方法即可。

具体应用参考示例项目phoenix-job-executor-embeddable、phoenix-job-executor-launcher或phoenix-job-executor-launcher-dubbo
