#### 简介
内嵌集成模式（JOB执行器和业务代码部署在同JVM的模式）的示例项目。<br/>

<b>`该模式适合执行器和定时任务服务采用集中式架构的项目。`</b>

#### POM依赖
	<properties>
		<phoenix.job.version>1.9.2-SNAPSHOT</phoenix.job.version>
		<job.executor.client.version>1.0.0-SNAPSHOT</job.executor.client.version>
	</properties>
		
	<dependency>
		<groupId>com.xuxueli</groupId>
		<artifactId>xxl-job-core</artifactId>
		<version>${phoenix.job.version}</version>
	</dependency>

	<dependency>
		<groupId>com.bestone.job</groupId>
		<artifactId>phoenix-job-executor-client</artifactId>
		<version>${job.executor.client.version}</version>
	</dependency>


#### 集成步骤

 * 1.继承"AbstractJobWorker"：“com.bestone.job.executor.service.api.AbstractJobWorker”，并实现任务执行方法doExecute；
 
 * 2.注册到Spring容器：添加“@Component”注解，被Spring容器扫描为Bean实例；
 
 * 3.注册到执行器工厂：添加“@JobWorker(value="自定义jobWorker名称")”注解，注解value值对应的是调度中心新建任务的JobWorker属性的值。
 
 * 4.执行日志：需要通过 "JobLogger.log" 打印执行日志，这里可以输入业务相关的一些参数，方便任务异常的时候排查问题。

	 例如：
	@JobWorker(value = "demoJobWorker1")
	@Component
	public class DemoJobWorker extends AbstractJobWorker {
	
		@Override
		public Result<String> doExecute(String param) throws Exception {
	
			// 模拟业务操作
			System.out.println("phoenix-job-executor-embeddable-->>>>job task handled business operation...");
			TimeUnit.SECONDS.sleep(2);
	
			JobLogger.log("PHOENIX-JOB execute successfully.");
			return SUCCESS;
		}
	}
	

 * 5.配置属性文件job-executor.properties。
	
	# web port
	server.port=8082
	
	### phoenix-job admin address list, such as "http://address" or "http://address01,http://address02"
	phoenix.job.admin.addresses=http://127.0.0.1:7777
	
	### phoenix-job executor address
	phoenix.job.executor.appname=fashang-standalone-job-executor-1
	phoenix.job.executor.ip=
	phoenix.job.executor.port=9988
	
	### phoenix-job, access token
	phoenix.job.accessToken=
	
	### phoenix-job log path
	phoenix.job.executor.logpath=/data/applogs/phoenix-job/jobhandler
	### phoenix-job log retention days
	phoenix.job.executor.logretentiondays=-1


 * 6.Spring配置文件applicationcontext-job.xml。
 	
	直接复制使用即可，需要注意：<context:component-scan base-package="修改为需要扫描自己项目的包路径"/>
  	
	<?xml version="1.0" encoding="UTF-8"?>
	<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
           http://www.springframework.org/schema/beans/spring-beans.xsd
           http://www.springframework.org/schema/context
           http://www.springframework.org/schema/context/spring-context.xsd">
	<bean id="propertyConfigurer" class="org.springframework.beans.factory.config.PropertyPlaceholderConfigurer">
		<property name="fileEncoding" value="utf-8" />
		<property name="locations">
			<list>
				<value>classpath*:job-executor.properties</value>
			</list>
		</property>
	</bean>
	<context:component-scan base-package="com.bestone.job.executor.service.jobworker" />
	<bean id="defaultJobExecutor" class="com.bestone.job.executor.service.core.DefaultJobExecutor" init-method="start" destroy-method="destroy" >
		<!-- 执行器注册中心地址[选填]，为空则关闭自动注册 -->
		<property name="adminAddresses" value="${phoenix.job.admin.addresses}" />
		<!-- 执行器AppName[选填]，为空则关闭自动注册 -->
		<property name="appName" value="${phoenix.job.executor.appname}" />
		<!-- 执行器IP[选填]，为空则自动获取 -->
		<property name="ip" value="${phoenix.job.executor.ip}" />
		<!-- 执行器端口号[选填]，为空则自动获取 -->
		<property name="port" value="${phoenix.job.executor.port}" />
		<!-- 访问令牌[选填]，非空则进行匹配校验 -->
		<property name="accessToken" value="${phoenix.job.accessToken}" />
		<!-- 执行器日志路径[选填]，为空则使用默认路径 -->
		<property name="logPath" value="${phoenix.job.executor.logpath}" />
		<!-- 日志保存天数[选填]，值大于3时生效 -->
		<property name="logRetentionDays" value="${phoenix.job.executor.logretentiondays}" />
	</bean>

</beans>

 * 7.webapp/WEB-INF/web.xml的配置

	<?xml version="1.0" encoding="UTF-8"?>
	<web-app xmlns="http://java.sun.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
		xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd"
		version="3.0">
		<display-name>phoenix-job-executor-embeddable</display-name>
		<context-param>
			<param-name>webAppRootKey</param-name>
			<param-value>phoenix-job-executor</param-value>
		</context-param>
		<context-param>
			<param-name>contextConfigLocation</param-name>
			<param-value>classpath*:applicationcontext-*.xml</param-value>
		</context-param>
		<listener>
			<listener-class>org.springframework.web.util.Log4jConfigListener</listener-class>
		</listener>
		<listener>
			<listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
		</listener>
		<welcome-file-list>
			<welcome-file>index.html</welcome-file>
		</welcome-file-list>
	</web-app>

#### 部署
打成war后部署在tomcat即可。
